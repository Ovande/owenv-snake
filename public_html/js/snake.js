/*------------------------------------------------------------------------------
 ===============================VARIABLES========================================
 ------------------------------------------------------------------------------*/
var snake;
var snakeLength;
var snakeSize;
var snakeDirection;
var snakeSpeed;

var food;

var Highscore = localStorage.getItem(HighscoreX);
var HighscoreX;

var context;
var srceenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;
var whaSound;
var bitrush;
var pictureF;
var pictureH;
var pictureH2;
var pictureB;
var Storage;

/*------------------------------------------------------------------------------
 ========================EXECUTING GAME CODE====================================
 -----------------------------------------------------------------------------*/
gameInitialize();
snakeInitialize();
foodInitialize();
setInterval(gameLoop, snakeSpeed);
/*------------------------------------------------------------------------------
 ============================GAME FUNCTIONS======================================
 -----------------------------------------------------------------------------*/
function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);

    gameOverMenu = document.getElementById("gameOver");
    centerMenuPosition(gameOverMenu);

    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);

    playHUD = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard");
    
    Highscore = document.getElementById("highscore");


    whaSound = new Audio("sounds/Sound effect - Wha Wha!.mp3");
    whaSound.preload = "auto";

    bitrush = new Audio("sounds/Bit Rush.mp3");
    bitrush.preload = "auto";

    pictureF = document.getElementById("food");
    pictureH = document.getElementById("head");
    pictureB = document.getElementById("body");
    pictureH2 = document.getElementById("head2");


    setState("PLAY");
}

function gameLoop() {
    gameDraw();
    drawScoreBoard();
    HighScore();
    if (gameState == "PLAY") {
        snakeUpdate();
        snakeDraw();
        foodDraw();
        localScore();
    }
}

function gameDraw() {
    context.fillStyle = "white";
    context.fillRect(0, 0, screenWidth, screenHeight);
    if (gameState == "PLAY") {
        bitrush.play();
    }
    else if (gameState == "GAME OVER") {
        bitrush.pause();
    }
}

function gameRestart() {
    snakeInitialize();
    foodInitialize();

    hideMenu(gameOverMenu);
    setState("PLAY");
}
/*------------------------------------------------------------------------------
 =========================SNAKE FUNCTIONS=======================================
 -----------------------------------------------------------------------------*/
function snakeInitialize() {
    snake = [];
    snakeLength = 1;
    snakeSize = 20;
    snakeDirection = "right";
    snakeSpeed = (1800 / 30);

    for (var index = snakeLength - 1; index >= 0; index--) {
        snake.push({
            x: index,
            y: 3
        });
    }
}

function snakeDraw() {
    for (var index = 0; 0 < index < 1; index++) {
        context.drawImage(pictureH, snake[index].x * snakeSize, snake[index].y * snakeSize, 20, 20);

        context.strokeStyle = "black";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth = 4;

        foodDraw();

    }
    if(snakeDirection == "left") {
        for (var index = 0; 1 < snake.length; index++) {
            context.drawImage(pictureB, (snake[index].x * snakeSize) + 20, (snake[index].y * snakeSize) + 0, 20, 20);
            foodDraw();
        }
    }
    else if(snakeDirection == "right") {
       for (var index = 0; 1 < snake.length; index++) {
            context.drawImage(pictureB, (snake[index].x * snakeSize) - 20, (snake[index].y * snakeSize) + 0, 20, 20);
            foodDraw();
        } 
    }
    else if(snakeDirection == "down") {
       for (var index = 0; 1 < snake.length; index++) {
            context.drawImage(pictureB, (snake[index].x * snakeSize) - 0, (snake[index].y * snakeSize) - 20, 20, 20);
            foodDraw();
        } 
    }
    else if(snakeDirection == "up") {
       for (var index = 0; 1 < snake.length; index++) {
            context.drawImage(pictureB, (snake[index].x * snakeSize) - 0, (snake[index].y * snakeSize) + 20, 20, 20);
            foodDraw();
        } 
    }
}


function snakeUpdate(snakeDraw) {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection == "down") {
        snakeHeadY++;
    }
    else if (snakeDirection == "right") {
        snakeHeadX++;
    }
    else if (snakeDirection == "up") {
        snakeHeadY--;
    }
    else if (snakeDirection == "left") {
        snakeHeadX--;
    }

    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);

    /*
     levelTwo();
     */

    snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
}
/*------------------------------------------------------------------------------
 ===========================FOOD FUNCTIONS======================================
 -----------------------------------------------------------------------------*/
function foodInitialize() {
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}

function foodDraw() {
    context.drawImage(pictureF, food.x * snakeSize, food.y * snakeSize, 20, 28);
}

function setFoodPosition() {
    var foodX = Math.floor(Math.random() * screenWidth);
    var foodY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(foodX / snakeSize);
    food.y = Math.floor(foodY / snakeSize);
}

/*------------------------------------------------------------------------------
 =========================INPUT FUNCTION========================================
 -----------------------------------------------------------------------------*/

function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }
    else if (event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }
    else if (event.keyCode == "38" && snakeDirection != "down") {
        snakeDirection = "up";
    }
    else if (event.keyCode == "37" && snakeDirection != "right") {
        snakeDirection = "left";
    }
}

/*------------------------------------------------------------------------------
 =============================COLLISION HANDLING================================
 -----------------------------------------------------------------------------*/

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y /*|| snakeHeadX+1 == food.x && snakeHeadY+1 || snakeHeadX-1 == food.x && snakeHeadY-1 == food.y*/) {
        snake.push({
            x: 0,
            y: 0
        });
        snakeLength++;

        foodDraw();
        setFoodPosition();
    }
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        setState("GAME OVER");
        whaSound.play();
    }

    else if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        setState("GAME OVER");
        whaSound.play();
    }
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for (var index = 1; index < snake.length; index++) {
        if (snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            setState("GAME OVER");
            whaSound.play();
            return;
        }
    }
}

/*------------------------------------------------------------------------------
 ========================= GAME STAtE HANDLING =================================
 -----------------------------------------------------------------------------*/

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*------------------------------------------------------------------------------
 =========================== MENU FUNCTIONS ====================================
 -----------------------------------------------------------------------------*/
function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";
}

function showMenu(state) {
    if (state == "GAME OVER") {
        displayMenu(gameOverMenu);
    }

    else if (state == "PLAY") {
        displayMenu(playHUD);
    }
}
function centerMenuPosition(menu) {
    menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
    menu.style.left = (screenWidth / 2.5) - (menu.offsetWidth / 2) + "px";
}
function drawScoreboard() {
    scoreboard.innerHTML = "Score: " + (snakeLength - 1);
    Highscore.innerHTML = "HighScore: " + localStorage.getItem(HighscoreX);
}

function HighScore() {
    if(localStorage.getItem(HighscoreX) < snakeLength) {
       localStorage.setItem(HighscoreX , snakeLength);
    }
    
}
function drawScoreBoard() {
    scoreboard.innerHTML = "Score:" + (snakeLength - 1);
}
var score;
var newScore;
score = (snakeLength - 1);
/*------------------------------------------------------------------------------
 *=================================SPECIAL======================================
 -----------------------------------------------------------------------------*/

function levelTwo() {
    if ((snakeLength - 1) >= 6) {
        snakeSpeed = (1000/30);
        snakeInitialize();
    }
}

/*
 * tests
 */
// Check browser support